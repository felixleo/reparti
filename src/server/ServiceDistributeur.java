

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;

public interface ServiceDistributeur extends Remote{

    public byte[] enregistrerImage(byte[] i, String[] servs) throws IOException,RemoteException, ServerNotActiveException, NotBoundException;

    public ArrayList<byte[]> decouper() throws IOException,RemoteException, ServerNotActiveException, NotBoundException;

    public byte[] recomposer(ArrayList<byte[]> tab) throws IOException,RemoteException, ServerNotActiveException, NotBoundException;

}
