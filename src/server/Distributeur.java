


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;

public class Distributeur implements ServiceDistributeur {

    private BufferedImage image;
    private String[] servs;
    private int nb;
    private int height;
    private int width;

    @Override
    public byte[] enregistrerImage(byte[] i, String[] servs) throws IOException, ServerNotActiveException, NotBoundException {
        InputStream inp = new ByteArrayInputStream(i);
        this.image = ImageIO.read(inp);
        this.servs = servs;
        this.nb= servs.length/2;
        this.height = this.image.getHeight();
        this.width = this.image.getWidth();
        ArrayList<byte[]> bt = decouper();
        return recomposer(bt);

    }

    @Override
    public ArrayList<byte[]> decouper() throws IOException, ServerNotActiveException, NotBoundException {
        //TODO tester de diviser une image en 3 avec une hauteur pair
        System.out.println(nb);
        int divWidth = image.getWidth()/nb;
        System.out.println(divWidth);
        ArrayList tab= new ArrayList<byte[]>();
        int j =0;
        for(int i = 0 ; i < this.nb ; i++){
            BufferedImage temp = this.image.getSubimage(i*divWidth,0,divWidth,this.image.getHeight());
            ByteArrayOutputStream baos= new ByteArrayOutputStream();
            ImageIO.write(temp, "jpg", baos);
            baos.flush();
            byte[] imageByte = baos.toByteArray();
            baos.close();
            tab.add(imageByte);
            Registry reg= LocateRegistry.getRegistry(servs[j], Integer.parseInt(servs[j+1]) );
            System.out.println("serveur"+ servs[j]+":"+servs[j+1]);
            j+=2;
            ServiceInverser srvinv = (ServiceInverser) reg.lookup("inversement");
            tab.set(i,srvinv.inverser((byte[])tab.get(i)));
            System.out.println("Image décomposée et recomposée");

        }

        return tab;

    }
    @Override
    public byte[] recomposer(ArrayList<byte[]> tab) throws IOException, ServerNotActiveException, NotBoundException {
        BufferedImage img = new BufferedImage(this.width,this.height,BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g = img.createGraphics();
        int taille = this.width/tab.size();
        for(int i = 0; i < tab.size() ; i++){
            InputStream inp = new ByteArrayInputStream(tab.get(i));
            BufferedImage img1 = ImageIO.read(inp);
            g.drawImage(img1,i*taille,0,null);
        }
        ByteArrayOutputStream baos= new ByteArrayOutputStream();
        ImageIO.write(img, "jpg", baos);
        baos.flush();
        byte[] imageByte = baos.toByteArray();
        baos.close();
        System.out.println("Image Recomposée");
        return imageByte;
    }


    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
}
