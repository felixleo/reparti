

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;

public interface ServiceInverser extends Remote {

    public byte[] inverser(byte[] img) throws IOException,RemoteException, ServerNotActiveException;
}
