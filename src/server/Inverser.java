

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;

public class Inverser implements ServiceInverser {

    @Override
    public byte[] inverser(byte[] img) throws IOException, ServerNotActiveException {
        InputStream inp = new ByteArrayInputStream(img);
        BufferedImage imgtrait = ImageIO.read(inp);
        byte[] imageByte =null;
        try {
            for (int i = 0; i<imgtrait.getWidth(); i++){
                for (int j = 0; j<imgtrait.getHeight(); j++){
                    Color pixelcolor = new Color(imgtrait.getRGB(i,j));

                    int r = pixelcolor.getRed();
                    int g = pixelcolor.getGreen();
                    int b = pixelcolor.getBlue();

                    r=Math.abs(r-255);
                    g=Math.abs(g-255);
                    b=Math.abs(b-255);

                    int rgb = new Color(r,g,b).getRGB();

                    imgtrait.setRGB(i,j,rgb);
                    ByteArrayOutputStream baos= new ByteArrayOutputStream();
                    ImageIO.write(imgtrait, "jpg", baos);
                    baos.flush();
                    imageByte = baos.toByteArray();
                    baos.close();
                    System.out.println("Partie inversée");
                }
            }

        } catch (Exception e) {
            System.err.println("erreur --> " +e.getMessage());
        }
        return imageByte;
    }
}
