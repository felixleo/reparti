

import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.*;

public class Server {

    public static void main(String[] args) throws RemoteException, ServerNotActiveException{

      //creer dist
      Distributeur t1 = new Distributeur();
      Inverser i = new Inverser();

      //en faire un service distant
      Object partie1 = UnicastRemoteObject.exportObject(t1, 0);
      Object inverser = UnicastRemoteObject.exportObject(i, 0);

      ServiceDistributeur serviceDistributeur1 = (ServiceDistributeur) partie1;
      ServiceInverser serviceInv = (ServiceInverser) inverser;

      System.out.println(partie1);
      System.out.println(inverser);

      //creer annuaire
      Registry reg = LocateRegistry.createRegistry(Integer.parseInt(args[0]));

      //enregistrer le service dans l'annuaire
      reg.rebind("distrib", serviceDistributeur1);
      reg.rebind("inversement",serviceInv);

    }
}
