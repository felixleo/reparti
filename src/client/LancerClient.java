

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.RemoteException;
import java.util.Base64;
import java.util.Scanner;

public class LancerClient {

    public static void LancerTraitement(String path, String ip, int port, int n) throws IOException, NotBoundException, ServerNotActiveException{

        byte[] imageByte;
        BufferedImage orig = ImageIO.read(new File(path));
        ByteArrayOutputStream baos= new ByteArrayOutputStream();
        ImageIO.write(orig, "jpg", baos);
        baos.flush();
        imageByte = baos.toByteArray();
        baos.close();

        Registry reg = LocateRegistry.getRegistry(ip, port);

        try{
            reg = LocateRegistry.getRegistry(ip, port);
        } catch(RemoteException e) {
            System.out.println("RemoteException annuaire non disponible");
        }

        try{
        ServiceDistributeur calc = (ServiceDistributeur) reg.lookup("distrib");
        String[] servs = new String[n*2];
        for(int i=0; i< n*2; i+=2){
            System.out.println("Entrez l'IP");
            Scanner sc= new Scanner(System.in);
            servs[i] = sc.nextLine();
            System.out.println("Entrez le port");
            Scanner sc1= new Scanner(System.in);
            servs[i+1] = sc1.nextLine();
        }
        byte [] imgret= calc.enregistrerImage(imageByte, servs);
        InputStream in = new ByteArrayInputStream(imgret);
        BufferedImage bImageFromConvert = ImageIO.read(in);

        ImageIO.write(bImageFromConvert, "jpg", new File(
                    "test.jpg"));

        } catch(RemoteException e) {
            System.out.println("RemoteException erreur connexion");
            e.printStackTrace();
        } catch(NotBoundException e) {
            System.out.println("NotBoundException");
            System.out.println("Service inexistant : " + e.getMessage());
        }

    }
    public static void main(String[] args) throws NotBoundException, ServerNotActiveException, IOException {
        LancerTraitement(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
    }
}
